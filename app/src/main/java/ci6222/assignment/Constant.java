package ci6222.assignment;

/**
 * Created by cinojose on 22/10/16.
 */
public class Constant {

    public static final String INITAL = "initial";
    public static final String REFRESH = "refresh";
    public static final String MAIN_TAG = "MAINACTIvity";
    public static final String FRAGMENT_TAG = "FRAGMENTTAG";
    public static final double NEARMEDISTANCE = 5;
    public static final double R = 6372.8;
}
