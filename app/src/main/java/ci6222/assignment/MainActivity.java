package ci6222.assignment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.Manifest;

import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     *
     * Static variables
     *
     */
    private static final String TAG = "ASSIGNMENT MAINACTIVITY";
    private static ArrayList<Traffic> trafficlistMain = new ArrayList<Traffic>();
    private static ArrayList<Traffic> trafficNearMe = new ArrayList<Traffic>();
    private static TrafficArrayAdapter tAdapater;
    private static TrafficArrayAdapter tNearmeAdapter;
    private ProgressDialog prd;
    private int temp = 0;
    private int currentposition=0;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private String isLoading="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"=====Inside on create======");
        setContentView(R.layout.activity_main);
        this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        loadData();
        synchronized(isLoading){
            try {
                isLoading.wait();
            }catch (Exception e){
                Log.e(TAG,"Exception in waiting"+e);
            }
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        //Loading the main data;

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                int position = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Refreshing..", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
                loadData();
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        loadData();
    }

    @Override
    public void onPause(){
        super.onPause();
        trafficlistMain.clear();
        Log.i(TAG,"====Inside on Pause====");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            loadData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends ListFragment {


        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String TAG ="MAINACTIVITY";
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final int request_code = 5;

        public PlaceholderFragment() {
        }



        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            int args = getArguments().getInt(ARG_SECTION_NUMBER);
            Log.i(TAG,"fragment"+args);
            loadUI();
            return rootView;
        }


        public void loadUI(){
            Log.i(TAG,ARG_SECTION_NUMBER);
            final int args = this.getArguments().getInt(ARG_SECTION_NUMBER);

                Log.i(TAG,"======loading mainlist=====");
                tAdapater = new TrafficArrayAdapter(this.getContext(), trafficlistMain);
                setListAdapter(tAdapater);



        }


        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            final int index = position;
            Log.i(TAG,"map activity");
            Intent mapIntent = new Intent(this.getContext(), MapActivity.class);
            final int args = this.getArguments().getInt(ARG_SECTION_NUMBER);

                mapIntent.putExtra("lat", trafficlistMain.get(index).getLatitude());
                mapIntent.putExtra("long", trafficlistMain.get(index).getLongitude());
                startActivityForResult(mapIntent, request_code);


        }

    }

    public static class NearmeFragment extends ListFragment {


        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String TAG ="MAINACTIVITY";
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final int request_code = 5;

        public NearmeFragment() {
        }



        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static NearmeFragment newInstance(int sectionNumber) {
            NearmeFragment fragment = new NearmeFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            int args = getArguments().getInt(ARG_SECTION_NUMBER);
            Log.i(TAG,"fragment"+args);
            loadUI();
            return rootView;
        }


        public void loadUI(){
            Log.i(TAG,ARG_SECTION_NUMBER);
            final int args = this.getArguments().getInt(ARG_SECTION_NUMBER);

                Log.i(TAG,"======loading nearme=====");
                tNearmeAdapter = new TrafficArrayAdapter(this.getContext(), trafficNearMe);
                setListAdapter(tNearmeAdapter);



        }


        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            final int index = position;
            Log.i(TAG,"map activity");
            Intent mapIntent = new Intent(this.getContext(), MapActivity.class);
            final int args = this.getArguments().getInt(ARG_SECTION_NUMBER);

                mapIntent.putExtra("lat", trafficNearMe.get(index).getLatitude());
                mapIntent.putExtra("long", trafficNearMe.get(index).getLongitude());
                startActivityForResult(mapIntent, request_code);


        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return PlaceholderFragment.newInstance(position + 1);
            }else{
                return NearmeFragment.newInstance(position + 1);
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Live Traffic";
                case 1:
                    return "Near Me";
            }
            return null;
        }
    }

    Handler mainHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String string = bundle.getString("myKey");
            Log.i(TAG,"****received message****"+string);
            if(string.equalsIgnoreCase(Constant.INITAL)){
                synchronized (isLoading){
                    isLoading.notify();
                }
            }else{
                Log.i(TAG,"Inside the else");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                            tAdapater.clear();
                            tAdapater.addAll(trafficlistMain);
                            tAdapater.notifyDataSetChanged();
                            Log.i(TAG,"updated near me list view");
                        tNearmeAdapter.clear();
                        tNearmeAdapter.addAll(trafficNearMe);
                        tNearmeAdapter.notifyDataSetChanged();



                    }
                });
            }
            prd.dismiss();

        }
    };

    public void loadData(){

        prd = ProgressDialog.show(this, "Refresh",
                "Refreshing content..", true);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                boolean isrefresh = trafficlistMain.size()==0? false:true;
                Utility utility = new Utility();
                String jsondata = utility.pollData();
                Log.i(TAG,jsondata);
                trafficlistMain.clear();
                trafficlistMain = utility.parseJson(jsondata);
                LocationClass mylocation = getCurrentLocation();
                if(mylocation!=null){
                    trafficNearMe = utility.getNearmeFromMain(trafficlistMain,mylocation.getLatitude(),
                            mylocation.getLongitude());
                    Log.i(TAG,"mylocation=="+mylocation);
                }else{
                    trafficNearMe = utility.getNearmeFromMain(trafficlistMain,1.3243269,103.8917603);
                }

                Log.i(TAG,"nearme="+trafficNearMe);
                /** TODO : For demo purpose
                 Traffic traffictemp = new Traffic();
                  traffictemp.setType("Roadwork");
                  traffictemp.setMessage("Message"+temp);
                  traffictemp.setLatitude(1003);
                  traffictemp.setLongitude(10.05);
                  trafficlistMain.add(0,traffictemp);
                  temp++;
                 */
                Message msg_data = mainHandler.obtainMessage();
                Bundle bundle_data = new Bundle();
                if(isrefresh){
                    bundle_data.putString("myKey",Constant.REFRESH);

                }else{
                    bundle_data.putString("myKey",Constant.INITAL);
                }

                msg_data.setData(bundle_data);
                mainHandler.dispatchMessage(msg_data);
            }
        };
        Thread polling = new Thread(runnable);
        polling.start();
    }

    public LocationClass getCurrentLocation(){
        LocationClass mylocation = null;
        try {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            mylocation = new LocationClass(location.getLatitude(), location.getLongitude());
        }catch (SecurityException e){
            Log.e(TAG,"No permission to detect location"+e);
        } catch (Exception e){
            Log.e(TAG,"Unable to detect the permission"+e);
        }
        return mylocation;

    }
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }



}
