package ci6222.assignment;

/**
 * Created by cinojose on 2/10/16.
 */
public class Traffic {

    private String type;
    private double longitude;
    private double latitude;
    private String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Traffic{" +
                "type='" + type + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", message='" + message + '\'' +
                '}';
    }
}
