package ci6222.assignment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by cinojose on 3/10/16.
 */
public class TrafficArrayAdapter extends ArrayAdapter<Traffic> {


    private ArrayList<Traffic> values;
    private Context context;

    public TrafficArrayAdapter(Context context, ArrayList<Traffic> values) {
        super(context, R.layout.fragment_main,values);
        this.values = values;
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.traffic_layout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
        textView.setText(values.get(position).getMessage());

        String type = values.get(position).getType();
        if(type.equalsIgnoreCase("Accident")){
            imageView.setImageResource(R.drawable.accident);
        }else if(type.equalsIgnoreCase("Roadwork")){
            imageView.setImageResource(R.drawable.road_work);
        }else if(type.equalsIgnoreCase("Vehicle breakdown")) {
            imageView.setImageResource(R.drawable.breakdown);
        } else if(type.equalsIgnoreCase("Heavy Traffic")) {
            imageView.setImageResource(R.drawable.traffic);
        }

        return rowView;

    }


}
