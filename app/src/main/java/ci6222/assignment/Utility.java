package ci6222.assignment;

import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by cinojose on 18/10/16.
 */
public class Utility {

    private  String APIKEY = "+ugu2jpnR5asOVsAu6cLXw==";
    private  String LTA_URL = "http://datamall2.mytransport.sg/ltaodataservice/TrafficIncidents";
    private  String TAG = "UTILTITY";

    public String pollData(){
        StringBuffer response = null;
        try {
            HttpURLConnection connection = null;
            URL url = new URL(LTA_URL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/json");
            connection.setRequestProperty("AccountKey", APIKEY);
            connection.setRequestProperty("Content-Length", "" + 0);
            int responseCode = connection.getResponseCode();
            Log.i(TAG,""+connection.getResponseCode());
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            }
        }
            catch(Exception e){

                Log.i(TAG,"Error in fetching data"+e.toString());
        }
        return response.toString();
    }


    public ArrayList<Traffic> parseJson(String jsondata) {

        ArrayList<Traffic> trafficlist = new ArrayList<Traffic>();
        try {
            JSONObject reader = new JSONObject(jsondata);
            JSONArray jsonArray = reader.optJSONArray("value");
            trafficlist.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject trafficJson = jsonArray.getJSONObject(i);
                Traffic traffic = new Traffic();
                traffic.setMessage(trafficJson.getString("Message"));
                traffic.setType(trafficJson.getString("Type"));
                traffic.setLatitude(Double.parseDouble(trafficJson.getString("Latitude")));
                traffic.setLongitude(Double.parseDouble(trafficJson.getString("Longitude")));
                trafficlist.add(traffic);
            }
        } catch (Exception e) {
            Log.i(TAG, "Error in loading json" + e.getMessage());
        }
        return trafficlist;
    }

    public ArrayList<Traffic> getNearmeFromMain(ArrayList<Traffic> mainList, double lat, double lon){

        ArrayList<Traffic> nearmeList = new ArrayList<Traffic>();
        double distance=0;
        for(Traffic tf : mainList){
            distance = haversine(tf.getLatitude(),tf.getLongitude(),lat,lon);
            if(distance<=Constant.NEARMEDISTANCE){
                nearmeList.add(tf);
            }
        }

        return nearmeList;
    }

    /*
     * Method to find the distance between two point in Km's
     */
    public static double haversine(double lat1, double lon1, double lat2, double lon2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return Constant.R * c;
    }


}
